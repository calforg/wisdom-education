package com.education.common.disabled;

/**
 * @author zengjintao
 * @version 1.0
 * @create_at 2020/3/24 20:07
 */
public @interface Enable {
}
